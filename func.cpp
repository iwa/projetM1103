//
//  func.cpp
//  Projet Semestre 1 Programmation
//  Bastien Broigniez & Pierre Bastard
//  Groupe D
//

void LireFiche(FICHE *ficheX) {
    cout << "Entrez le nom de l'etudiant : ";
    cin >> (*ficheX).nom;
    cout << "Prenom : ";
    cin >> (*ficheX).prenom;
    cout << "Sa date de Naissance :" << endl << "Jour : ";
    cin >> (*ficheX).date_naissance.jour;
    cout << "Mois : ";
    cin >> (*ficheX).date_naissance.mois;
    cout << "Annee : ";
    cin >> (*ficheX).date_naissance.annee ;
    cout <<"Sa formation : ";
    cin >> (*ficheX).info;
    cout << "Est-il redoublant (O:Oui, N:Non) ? ";
    cin >> (*ficheX).redoublant;
    cout << "Son groupe : ";
    cin >> (*ficheX).groupe;
    (*ficheX).nbnotes = 0;
}

void EcrireFiche(FICHE ficheX) {
    cout << endl << "La fiche de " << ficheX.nom << " " << ficheX.prenom << endl;
    cout <<"\tSa date de naissance est le " << ficheX.date_naissance.jour << " " << ficheX.date_naissance.mois << " " << ficheX.date_naissance.annee << endl;
    cout << "\tSa formation : " << ficheX.info<<endl;
    cout << "\tSon groupe : " << ficheX.groupe<<endl;
    cout << "\tIl est redoublant : ";
    if (ficheX.redoublant == 'O') cout<<"Oui"; else cout<<"Non";
    cout << "\nSes notes: \n";
    for (int i = 0; i < ficheX.nbnotes; i++)
        cout << ficheX.notes[i] << "\t";
    cout << endl;
}

int ajoutNote(FICHE *ficheX, float Nx) {
    if((*ficheX).nbnotes < MAXNOTES) {
        (*ficheX).notes[(*ficheX).nbnotes] = Nx;
        (*ficheX).nbnotes++;
        cout << "La note (" << Nx << ") a bien été ajoutée à la fiche de " << (*ficheX).nom << " " << (*ficheX).prenom << endl;
        cout << "Cette personne a desormais " << (*ficheX).nbnotes << " notes" << endl;
        return 1;
    } else {
        cout << "Ajout de la note impossible, nombre maximal de notes atteint !" << endl;
        return 0;
    }
}

float moyenneNotes(FICHE ficheX) {
    float totalNotes = 0;
    for(int i = 0; i < ficheX.nbnotes; i++) {
        totalNotes += ficheX.notes[i];
    }
    return (totalNotes / ficheX.nbnotes);
}

void initListFiche(FICHE *LFich, int NBFich) {
    for(int i = 0; i < NBFich; i++)
        LireFiche(&LFich[i]);
}

float totalMoyenne(FICHE *LFich, int NBfirst) {
    float totalNotes = 0, totalMoy = 0;
    for(int i = 0; i < NBfirst; i++) {
        for(int j = 0; j < LFich[i].nbnotes; j++) {
            totalNotes += LFich[i].notes[j];
        }
        totalMoy += (totalNotes / LFich[i].nbnotes);
        totalNotes = 0;
    }
    return (totalMoy / NBfirst);
}