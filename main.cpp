//
//  main.cpp
//  Projet Semestre 1 Programmation
//  Bastien Broigniez & Pierre Bastard
//  Groupe D
//

#include <iostream>
#include <stdlib.h>

#define N 100
#define MAXNOTES 20
#define MAXFICHES 10

using namespace std;

typedef struct {
    int jour;
    char mois[10];
    int annee;
} DATE;

typedef struct {
    char nom[10];
    char prenom[10];
    DATE date_naissance;
    char info[3];
    char redoublant;
    int groupe;
    float notes[MAXNOTES];
    int nbnotes;
} FICHE;

#include "func.cpp"
#include "func.h"

int main(int argc, const char * argv[]) {

    //
    //  NOTE :
    //  On a ici utlisé une tableau contenant des fiches pour LFich,
    //  mais il est possible de créer une structure de la manière suivante :
    //
    //  typedef struct {
    //      FICHE LFich[MAXFICHES];
    //  } LISTFICHE;
    //
    //  Mais il est plus optimisé d'utiliser directement un tableau
    //
    FICHE fichEtud, LFich[MAXFICHES];

    // Rentre les informations d'une fiche
    LireFiche(&fichEtud);

    // Affiche les informations d'une fiche
    EcrireFiche(fichEtud);

    // Ajoute deux notes
    ajoutNote(&fichEtud, 15);
    ajoutNote(&fichEtud, 16);

    // Raffiche les informations d'une fiche
    EcrireFiche(fichEtud);

    // Calcule et affiche la moyenne des notes d'une fiche
    cout << "Moyenne : " << moyenneNotes(fichEtud) << endl;

    // Initialise des fiches dans un tableau de fiches
    initListFiche(LFich, 3);

    // Ajoute des notes à ces fiches
    ajoutNote(&LFich[0], 15);
    ajoutNote(&LFich[0], 16);
    ajoutNote(&LFich[1], 10);
    ajoutNote(&LFich[1], 11);
    ajoutNote(&LFich[2], 15);
    ajoutNote(&LFich[2], 18);

    // Moyenne totale des notes des fiches
    cout << "Moyenne Totale : " << totalMoyenne(LFich, 3) << endl;

    return 0;
}
