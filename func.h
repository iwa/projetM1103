//
//  func.h
//  Projet Semestre 1 Programmation
//  Bastien Broigniez & Pierre Bastard
//  Groupe D
//

void LireFiche(FICHE *ficheX);
void EcrireFiche(FICHE ficheX);
int ajoutNote(FICHE *ficheX, float Nx);
float moyenneNotes(FICHE ficheX);
void initListFiche(FICHE *LFich, int NBFich);
float totalMoyenne(FICHE *LFich, int NBfirst);